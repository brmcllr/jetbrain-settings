#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from rest_framework import serializers


class Serializer(serializers.HyperlinkedModelSerializer):
    # remember this is basically like a form
    = serializers.(default=)    
    
    class Meta:
        model = 
        fields = ()
        
    def create(self, data):
        # data is a dictionary of value
        # you should return something from .create()
        return 
        
    def validate(self, data):
        return data
        
    def validate_(self, value):
        # "clean" data from a field
        return value 